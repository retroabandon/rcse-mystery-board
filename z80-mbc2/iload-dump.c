/*
 *  iload-dump: Dump data about/of the binary embedded in `iload.c`.
 *
 *  With no arguments, this prints the start address and length. With
 *  any argument, this dumps the embedded binary code.
 */

#include <stdint.h>
#include <stdio.h>

typedef uint16_t word;
typedef uint8_t byte;
#define PROGMEM

#include "iload.c"

int main(int argc, char **argv) {
    int datalen = sizeof(boot_B_);
    if (argc == 1) {
        //  No arguments: print start and length.
        printf("start=$%04X length=$%04X (%d)\n",
            boot_B_StrAddr, datalen, datalen);
    } else {
        // Any args given: dump data to stdout.
        for ( int i=0; i<datalen; i++ ) {
            printf("%c", boot_B_[i]);
        }
    }
}
