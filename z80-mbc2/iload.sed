#   Comments and annotations for iload.bin.
#   Requires `z80dasm -a` and `sed -E`.
#   https://www.gnu.org/software/sed/manual/sed.html

s/\t;fd51/\n/
s/\t;fdf3/\n/
s/\t;fea0/\n/
s/\t;febd/\n/
s/\t;fec5/\n/
s/\t;fed5/\n/
s/\t;fee0/\n/
s/\t;fef3/\n/
s/\t;ff05/\n/
s/\t;ff05/\n/
s/\t;ff19/\n/
s/\t;ff22/\n/
s/\t;ff2d/\n/
s/\t;ff3d/\n/
s/\t;ff4a/\n/
s/\t;ff57/\n/
s/\t;ff68/\n/
s/\t;ff71/\n/
