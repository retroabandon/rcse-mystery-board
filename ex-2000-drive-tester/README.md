EX 2000 Disk Drive Tester
=========================

From `darkhelmet` in the 8 Bit World Discord server.

Labels:
- "DISK DRIVE TESTER / EX 2000"
- "systems circuits service"
- "PROTO PC INC. ©1983"

#### Hardware

LSI:
- Toshiba [TC5517APL-2][] (DIP-24W) static 2K×8 static RAM, 200 ns.
- Synertek [SY6502A][] (DIP-40W) CPU.
- Synertek [SY6522A][] (DIP-40W) Versatile Interface Adapter (VIA).
- 2K×8 ROM (on back; not in photos)

74xx:
- [M74LS10P][] (DIP-14N) triple 3-input NAND gate.
- [DM7416N][] (DIP-14N) open collector hex inverter.
- [SN74LS14N][] (DIP-14N) Schmitt-trigger hex inverter.

Display and input:
- 5 red LEDs, in a column.
- RCA [CD4511B][] (DIP-16N, ×2) BCD-to-7-Segment Latch Decoder Drivers and
  two-digit 7-segment plus decimal LED display (150 Ω current-limiting
  resistors).
- 16 pushbuttons (probably on a 4×4 matrix).

Connectivity:
- 50-pin male DIP header, shrouded, locking
- 34-pin male DIP header, shrouded, locking
- 4-pin male drive power connector

#### User Interface Labels

LEDs, from top to bottom:
- Track 0
- Index
- Side
- Ready
- Write Protect

Buttons:
- Top row: Side, speed, Hyst, Motor On, Erase, Write, Step Rate, Drive Type
- Bot row: TRK 0, Index, Cats Eye, Read Res, Azim, Top Trk, ←, →

#### Memory map (tentative, incomplete)

    $F800 - $FFFF   2K ROM
    $1C00           6522 VIA (16 registers)
    $0000 - $0800   2K RAM


Disassembly
-----------

### dis6502 Disassembler

The initial disassembly is done with [Steve Fosdick's version of
`dis6502`][fosdick]. (This is based on [Peter Froehlich's version][phf],
which contains a few more commits since the Fosdick fork.)

Currently you'll need a compiled version of `dis6502` in your path to run
the `Test` script. To build it on Debian you'll need the following packages:
- `build-essential`: for `make`, the C compiler, etc.
- `flex`: for `lex`.
- `docutils-common`: for `rst2man`, called as `rst2man.py` in the Makefile.

Typical tweaks to the dis6502 `Makefile` include:
- Change the `dis6502.1` target to run `rst2man` instead of `rst2man.py`
- Change `INSTDIR` to your preferred $PREFIX.

### Test Script

Running `./Test` will generate a new disassembly file, `rom-dos-1.dis`,
containing the addresses and data values at the left and a cross-reference
table at the end of the file. This is currently committed to the repo for
easy reference via web views on GitLab. The predefines file,
`rom-dos-1.def`, contains the start addresses for tracing and some known
symbol names.

We will later use the `dis6502 -a` option to generate a file suitable for
input to an assembler and further reverse-engineering will happen there.
That should be tested by assembling the binary and comparing it to the
original binaries.



<!-------------------------------------------------------------------->
[SY6522A]: ./datasheet/w65c22.pdf
[TC5517APL-2]: ./datasheet/TOSCS32888-1.pdf
[CD4511B]: ./datasheet/cd4511b.pdf
