Board Hardware Description
==========================

See also:
- The board photographs under [`Pictures/`].
- The [original post][12501].
- Comments in the disassemblies and their .info files.

ICs:
- Y1: 4 MHz crystal. The clock derived from this is divided by four to
  produce the CPU's 1 MHz system clock.
- U29: 6809 CPU.
- U31: 2K of static RAM, an AMD AM9128-20PC, mapped to location $0000.
  See also the "reset" comments in the `.info` files for more information.
- U32: 2K ROM mapped to location $F800.
- U33: Empty socket for a second 2K ROM.
- U28: 6850 EF6850P ACIA.
- U27: EF6821P PIA,`P16` in the disassembly.
- U26, U25: EF6821P PIAs, `P32` in the disassembly.

Configuration switches:
- S1: 16-position rotary switch marked `0` through `F` (near ACIA)
- 4-switch DIP switch block S2 (near ROM):
  - 0: (no label)
  - 1: TEST/OPER.
  - 2: 300/1200 BAUD  (on bit 4 of P16a_DR; low=1200, high=300)
  - 3: AUX
- S3: "RESTART": empty position for a normally-open switch

Cable connections:
- J1: female DB-37
- J2: female DB-25
- J3: female DB-25
- J4: male 50-pin connector for IDC plug (as used w/ribbon cable)


EF6850P ACIA
------------

Register summary from the [data sheet][EF6850P]:

    0 RD: status
          0:   receive data register full (RDRF)
          1:   transmit data register empty (TDRE)
          2:   data carrier detect (/DCD)
          3:   clear to send (/CTS)
          4:   framing error (FE)
          5:   receiver overrun (OVRN)
          6:   parity error (PE)
          7:   interrupt request (/IRQ)
    0 WR: control register (CR)
          0-1: counter divide:  00=÷1  01=÷16  10=÷64  11=reset ACIA
          2-4: word select: 000=7E2  001=7O2  010=7E1  011=7O1
                            100=8-2  101=8-1  110=8E1  111=8O1
          5-6: transmit control
          7:   receive interrupt enable
    1 RD: receive data register (RDR)
          0-7: data byte (parity bit not included)
    1 WR: transmit data register (TDR)
          0-7: data byte


EF6821P PIA ×3
--------------

Interface and register summary from the [data sheet][EF6821P]:

    External Interfaces (B is same as A):
        PA0-PA7: data lines read/written via DR (set direction w/DDR)
        CA1: input only, generates interrupt
        CA2: input (generates interrupt) or output (internal pullup)
    Registers:
        0: Port A data register (DRA) and data direction register (DDRA)
        1: Port A control register (CRA)
        2: Port B data register (DRB) and data direction register (DDRB)
        3: Port B control register (CRB)
    DDR: 0=input (internal pullup) 1=output
        At reset all registers are zeroed, so all lines will be inputs.
        Reading an output line gives the current voltage on that pin, matching
        the ouput register value so long as the output drivers can maintain
        >2.0 V or <0.8 V on the output pin.
    Control register bits (CBn resp.):
            0 RW:   0=interrupts disabled; 1=interrupts enabled
            1 RW:   Interrupt trigger transition CA1 0=high→low 1=low→high
            2 RW:   0=DDR selected; 1=DR selected
            3 RW:   CA2 setting (complex, see data sheet)
            4 RW:   CA2 setting (complex, see data sheet)
            5 RW:   0=CA2 is input; 1=CA2 is output
            6 RO:   Interrupt flag CA1 line (cleared by DR read)
            7 RO:   Interrupt flag CA2 line (cleared by DR read)


IRQ Lines
---------

From an examination of the board photographs, P16 `/IRQB` (U27 pin 37)
ACIA `/IRQ` (U28 pin 7) are connected together via a trace on the back
side, and through a via to the front side that then leads to the 6809
`/IRQ` line (U29 pin 3).

There are no back-side traces for 6809 `/IRQ`. There may be a
front-side trace that continues on under the chip from the above, but
it seems unlikely as the code configures only P16 and the ACIA to
generate interrupts.


<!-------------------------------------------------------------------->
[12501]: https://retrocomputing.stackexchange.com/q/12501/7208
[EF6821P]: https://archive.org/details/6821_PIA_Datasheet
[EF6850P]: https://www.cpcwiki.eu/imgs/3/3f/MC6850.pdf
[`Pictures/`]: Pictures/
