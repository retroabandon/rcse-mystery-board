          
//                  A0   A1   A2   A3   A4   A5   A6   A7   A8   A9   A10
//                 ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----
int addrPins[] = {   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13  };

//                  D0   D1   D2   D3   D4   D5   D6   D7
//                 ---- ---- ---- ---- ---- ---- ---- ----
int dataPins[] = {   0,   2,  18,  19,  20,  21,  22,  23  };


//------------------------------------------------------------------------------
// setup
//
void setup() {

  Serial.begin(9600);

  //Setup address pins for output
  for(int p = 0; p <= 10; p++) {
    pinMode(addrPins[p], OUTPUT);
  }

  //Setup data pins for input
  for(int p = 0; p <= 7; p++) {
    pinMode(dataPins[p], INPUT);
  }

  delay(5000);
   
}


//------------------------------------------------------------------------------
// loop
//
void loop() {

  while(true) {
    Serial.print("START-START-START");
  for (int address = 0; address < 2048; address++) {
 
    writeAddress(address);
    delay(10);
    
    byte data = readData();
    Serial.write(data);
    
  }
  Serial.print("END-END-END");
  }
}


//------------------------------------------------------------------------------
// readData
//
byte readData() {

  int result = 0;
  
  for (int n = 0; n <= 7; n++)
  {

    int val = digitalRead(dataPins[n]);
    if (val == HIGH)
    {
      bitWrite(result, n, 1);
    }
    
  }

  return (byte)result;
  
}


//------------------------------------------------------------------------------
// writeAddress
//
void writeAddress(int address) {

  //Write A0 to A10
  for (int p = 10; p >= 0 ; p--) {

    bool bit = bitRead(address, p);
  
    if (bit) {
      digitalWrite(addrPins[p], HIGH);
    }
    else {
      digitalWrite(addrPins[p], LOW);
    }

  }
  
}
