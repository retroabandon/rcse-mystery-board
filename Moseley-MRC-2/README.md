This repo contains materials related to the Retrocomputing Stack
Exchange question, ["Can anyone identify this Parallel to Serial
Interface Board?"][rc 12501].

[`Moseley-MRC-2-vol2.pdf`] appears to be the manual for two devices
using the two boards bought by the poster: the MSD-1 Multiple Status
Display and the MDC-2 Multiple Direct Command Option.


Disassembly
-----------

A slightly-tweaked version of [f9dasm] is set up as a submodule; the
[`Makefile`] knows how to update and build this before using f9dasm to
generate the disassembly.

The disassembler reads the `.info` files for disassembly hints and
comments that it uses when generating the disassembly. For more
details on the format of this file, see [`f9dasm/f9dasm.htm`]

We commit the disassembly for two reasons: so that people who can't
run it can still see it in the GitHub web interface and so that we can
track the changes we've made to the generated output via the info
file. Note that the address/hex values/ASCII values comments are
turned off in the committed version of the disassembly. You're likely
going to want to turn these on from time to time; please ensure you
turn the options off and re-run the disassembly before commiting to
avoid a massve, unreadable diff of the committed output file.

For details of the steps taken to improve the disassembly, read
through the commit logs of this Git repo, starting from the beginning.


Board Information
-----------------

The board is marked as follows.

- Silkscreen-printed on top is a stylized "MA" logo and:

      PARALLEL TO SERIAL INTFC BD
      51C5959-21
      ASSY 20D2844

- A dot-matrix-printed sticker in the corner says `9204439-D`.
- Silkscreen-printed on the bottom is `51C5959-11`.

For switch, connector and IC details, see the [`Hardware`] file.


ROM Information
---------------

There were two identical boards, but with different ROMs. These were:
- `26A1063-A (PS-1) (MSD-1)`, in `PS-1.bin`.
- `26A1064-A (PD-1) (MDC-2)`, in `PD-1.bin`


<!-------------------------------------------------------------------->
[`Moseley-MRC-2-vol2.pdf`]: ./Moseley-MRC-2-vol2.pdf

[`f9dasm/f9dasm.htm`]: f9dasm/f9dasm.htm
[`Hardware``]: Hardware.md
[`Makefile`]: Makefile

[rc 12501]: https://retrocomputing.stackexchange.com/q/12501/7208
[f9dasm]: https://github.com/0cjs/f9dasm
